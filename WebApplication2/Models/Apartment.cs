﻿namespace WebApplication2.Models
{
    public partial class Apartment
    {
        public int IdApartment { get; set; }
        public string Name { get; set; }
        public decimal? Size { get; set; }
        public int? Rooms { get; set; }
        public bool? Parking { get; set; }
    }
}
