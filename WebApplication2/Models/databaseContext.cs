﻿using Microsoft.EntityFrameworkCore;

namespace WebApplication2.Models
{
    public partial class databaseContext : DbContext
    {
        public databaseContext()
        {
        }

        public databaseContext(DbContextOptions<databaseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Apartment> Apartment { get; set; }
        public virtual DbSet<Guest> Guest { get; set; }
        public virtual DbSet<Reservation> Reservation { get; set; }
        public virtual DbSet<Room> Room { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseMySql(Const.mySqlUrl, x => x.ServerVersion("10.1.38-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Apartment>(entity =>
            {
                entity.HasKey(e => e.IdApartment)
                    .HasName("PRIMARY");

                entity.ToTable("apartment");

                entity.Property(e => e.IdApartment)
                    .HasColumnName("idApartment")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Parking).HasColumnName("parking");

                entity.Property(e => e.Rooms)
                    .HasColumnName("rooms")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasColumnType("decimal(10,0)");
            });

            modelBuilder.Entity<Guest>(entity =>
            {
                entity.HasKey(e => e.IdGuest)
                    .HasName("PRIMARY");

                entity.ToTable("guest");

                entity.Property(e => e.IdGuest)
                    .HasColumnName("idGuest")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Address)
                    .HasColumnName("address")
                    .HasColumnType("varchar(500)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Age)
                    .HasColumnName("age")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Surname)
                    .HasColumnName("surname")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");
            });

            modelBuilder.Entity<Reservation>(entity =>
            {
                entity.HasKey(e => new { e.IdGuest, e.IdRoom, e.StartDate })
                    .HasName("PRIMARY");

                entity.ToTable("reservation");

                entity.Property(e => e.IdGuest)
                    .HasColumnName("idGuest")
                    .HasColumnType("int(11)");

                entity.Property(e => e.IdRoom)
                    .HasColumnName("idRoom")
                    .HasColumnType("int(11)");

                entity.Property(e => e.StartDate)
                    .HasColumnName("startDate")
                    .HasColumnType("date");

                entity.Property(e => e.EndDate)
                    .HasColumnName("endDate")
                    .HasColumnType("date");
            });

            modelBuilder.Entity<Room>(entity =>
            {
                entity.HasKey(e => e.IdRoom)
                    .HasName("PRIMARY");

                entity.ToTable("room");

                entity.Property(e => e.IdRoom)
                    .HasColumnName("idRoom")
                    .HasColumnType("int(11)");

                entity.Property(e => e.AirConditioner).HasColumnName("airConditioner");

                entity.Property(e => e.IdApartment)
                    .HasColumnName("idApartment")
                    .HasColumnType("int(11)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("latin1")
                    .HasCollation("latin1_swedish_ci");

                entity.Property(e => e.Size)
                    .HasColumnName("size")
                    .HasColumnType("int(4)");

                entity.Property(e => e.Tv).HasColumnName("tv");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
