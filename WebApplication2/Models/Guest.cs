﻿namespace WebApplication2.Models
{
    public partial class Guest
    {
        public int IdGuest { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public int? Age { get; set; }
    }
}
