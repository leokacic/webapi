﻿using System;

namespace WebApplication2
{
    public class ReservationData
    {
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public String guestName { get; set; }
        public String guestSurname { get; set; }
        public String roomName { get; set; }
        public String apartmentName { get; set; }
    }
}