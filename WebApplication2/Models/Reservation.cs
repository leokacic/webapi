﻿using System;

namespace WebApplication2.Models
{
    public partial class Reservation
    {
        public int IdGuest { get; set; }
        public int IdRoom { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
    }
}
