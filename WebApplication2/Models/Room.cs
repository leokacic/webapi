﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication2.Models
{
    public partial class Room
    {
        public int IdRoom { get; set; }
        public int IdApartment { get; set; }
        public string Name { get; set; }
        public int Size { get; set; }
        public bool Tv { get; set; }
        public bool AirConditioner { get; set; }
        [NotMapped]
        public string ApartmentName { get; set; }
    }
}
