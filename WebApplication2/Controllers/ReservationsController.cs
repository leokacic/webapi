﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReservationsController : ControllerBase
    {
        private readonly databaseContext _context;

        public ReservationsController(databaseContext context)
        {
            _context = context;
        }

        // GET: api/Reservations
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Reservation>>> GetReservation()
        {
            //System.Diagnostics.Debug.WriteLine("aaaa ---- " + r);
            return await _context.Reservation.ToListAsync();
        }

        // GET: api/Reservations/data
        [HttpGet("data")]
        public List<ReservationData> GetReservationData()
        {
            List<ReservationData> list = new List<ReservationData>();

            var reservations = from res in _context.Reservation
                               join room in _context.Room on res.IdRoom equals room.IdRoom
                               join guest in _context.Guest on res.IdGuest equals guest.IdGuest
                               join apt in _context.Apartment on room.IdApartment equals apt.IdApartment
                               select new
                               {
                                   guestName = guest.Name,
                                   guestSurname = guest.Surname,
                                   roomName = room.Name,
                                   apartmentName = apt.Name,
                                   startDate = res.StartDate,
                                   endDate = res.EndDate
                               };

            foreach (var reservation in reservations)
            {
                ReservationData resData = new ReservationData();
                resData.guestName = reservation.guestName;
                resData.guestSurname = reservation.guestSurname;
                resData.StartDate = reservation.startDate;
                resData.EndDate = reservation.endDate;
                resData.apartmentName = reservation.apartmentName;
                resData.roomName = reservation.roomName;
                list.Add(resData);
            }
            return list;
        }

        // GET: api/Reservations/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Reservation>> GetReservation(int id)
        {
            var reservation = await _context.Reservation.FindAsync(id);

            if (reservation == null)
            {
                return NotFound();
            }

            return reservation;
        }

        // PUT: api/Reservations/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutReservation(int id, Reservation reservation)
        {
            if (id != reservation.IdGuest)
            {
                return BadRequest();
            }

            _context.Entry(reservation).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ReservationExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Reservations
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Reservation>> PostReservation(Reservation reservation)
        {
            _context.Reservation.Add(reservation);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (ReservationExists(reservation.IdGuest))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetReservation", new { id = reservation.IdGuest }, reservation);
        }

        // DELETE: api/Reservations/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Reservation>> DeleteReservation(int id)
        {
            var reservation = await _context.Reservation.FindAsync(id);
            if (reservation == null)
            {
                return NotFound();
            }

            _context.Reservation.Remove(reservation);
            await _context.SaveChangesAsync();

            return reservation;
        }

        private bool ReservationExists(int id)
        {
            return _context.Reservation.Any(e => e.IdGuest == id);
        }
    }
}
