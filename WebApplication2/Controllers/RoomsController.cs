﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoomsController : ControllerBase
    {
        private readonly databaseContext _context;

        public RoomsController(databaseContext context)
        {
            _context = context;
        }

        // GET: api/Rooms
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Room>>> GetRoom()
        {
            return await _context.Room.ToListAsync();
        }

        // GET: api/Rooms/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Room>> GetRoom(int id)
        {
            var room = await _context.Room.FindAsync(id);

            if (room == null)
            {
                return NotFound();
            }

            return room;
        }

        // GET: api/Rooms/data
        [HttpGet("data")]
        public List<Room> GetRoomData()
        {
            List<Room> list = new List<Room>();

            var rooms = from room in _context.Room
                        join apt in _context.Apartment on room.IdApartment equals apt.IdApartment
                        select new
                        {
                            roomId = room.IdRoom,
                            roomName = room.Name,
                            roomSize = room.Size,
                            roomTv = room.Tv,
                            roomAirConditioner = room.AirConditioner,
                            apartmentId = apt.IdApartment,
                            apartmentName = apt.Name,
                        };

            foreach (var room in rooms)
            {
                Room resData = new Room();
                resData.IdRoom = room.roomId;
                resData.Name = room.roomName;
                resData.Size = room.roomSize;
                resData.Tv = room.roomTv;
                resData.AirConditioner = room.roomAirConditioner;
                resData.IdApartment = room.apartmentId;
                resData.ApartmentName = room.apartmentName;
                list.Add(resData);
            }
            return list;
        }

        // PUT: api/Rooms/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutRoom(int id, Room room)
        {
            if (id != room.IdRoom)
            {
                return BadRequest();
            }

            _context.Entry(room).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoomExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Rooms
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Room>> PostRoom(Room room)
        {
            _context.Room.Add(room);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetRoom", new { id = room.IdRoom }, room);
        }

        // DELETE: api/Rooms/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Room>> DeleteRoom(int id)
        {
            var room = await _context.Room.FindAsync(id);
            if (room == null)
            {
                return NotFound();
            }

            _context.Room.Remove(room);
            await _context.SaveChangesAsync();

            return room;
        }

        private bool RoomExists(int id)
        {
            return _context.Room.Any(e => e.IdRoom == id);
        }
    }
}
